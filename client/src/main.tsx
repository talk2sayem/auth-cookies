import { Theme } from '@radix-ui/themes';
import React from 'react';
import ReactDOM from 'react-dom/client';
import { Toaster } from 'sonner';
import App from './App.tsx';
// css
import '@radix-ui/themes/styles.css';
import './index.css';


ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <Theme accentColor='iris' radius='small' scaling="95%" appearance="dark" >
      <App />
      <Toaster />
    </Theme>
  </React.StrictMode>,
)
