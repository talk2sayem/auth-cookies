const express = require('express');
const app = express();
const cookieParser = require('cookie-parser');

require('dotenv').config()
app.use(require('cors')({
    origin: [process.env.CLIENT_URI, process.env.CLIENT_URI2],
    credentials: true
}));
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser());
// home route

app.get('/', (req, res) => {
    res.send('Welcome to the server');
});

// routes
// set cookie
app.get('/set-cookie', (req, res) => {
    res.cookie('username', 'john doe', {
        maxAge: 1000 * 5, // 5 seconds
        // expires: new Date(Date.now() + 1000 * 5), // 5 seconds
        httpOnly: true,
        secure: true,
        sameSite: 'None',
    });
    res.status(200).json({ message: 'Cookie has been set' });
});

// get cookie
app.get('/get-cookie', (req, res) => {
    res.status(200).json({ message: "Your cookies is", cookie: req.cookies });
});
// clear cookie
app.get('/clear-cookie', (req, res) => {
    res.cookie('username', '', {
        maxAge: 0,
        // expires: new Date(0),
        httpOnly: true,
        secure: true,
        sameSite: 'None',
    })
    res.status(200).json({ message: 'Cookie has been cleared' });
});



// listen on port
const port = process.env.PORT || 5000;
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});