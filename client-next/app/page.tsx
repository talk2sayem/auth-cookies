"use client"
import { Button, Card, Container, Flex } from "@radix-ui/themes";
import { useState } from "react";
import { toast } from 'sonner';

const style = { flex: 1, whiteSpace: 'nowrap' };
const serverURL = process.env.NEXT_PUBLIC_SERVER_URI;

export default function App() {
  const [loading, setLoading] = useState(false);

  const handleCookie = async (action: 'set' | 'get' | 'clear') => {
    setLoading(true);
    try {
      const res = await fetch(`${serverURL}/${action}-cookie`, { credentials: 'include' });
      const data = await res.json();
      toast.success(data?.message, { description: JSON.stringify(data?.cookie) })
      setLoading(false);
    } catch (err: any) {
      setLoading(false);
      console.log({ err })
      toast.error('An error occurred', { description: err?.message })
    }
  }

  return (
    <Container p="4">
      <Card>
        <Flex direction="row" gap="3" wrap="wrap" >
          <Button disabled={loading} style={style} onClick={() => handleCookie('set')} variant="surface">Set Cookie</Button>
          <Button disabled={loading} style={style} onClick={() => handleCookie('get')} variant="surface">Get Cookie</Button>
          <Button disabled={loading} style={style} onClick={() => handleCookie('clear')} variant="surface">Clear Cookie</Button>
        </Flex>
      </Card>
    </Container>
  )
}
