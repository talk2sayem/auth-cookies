# JWT-Auth cookie 
## About project
This is a simple project to demonstrate how cookies are set in client's browser from the server. 

## How to run
 - Clone the project
- Navigate to the client and server folder and run `pnpm i`
- Run the client and server by running `pnpm dev`
- Open the browser and go to `http://localhost:5173`

## How to use
- Open the browser and go to `http://localhost:5173`
- Click on the `Set Cookie` button to set the cookie in the browser
- Click on the `Get Cookie` button to get the cookie from the browser
- Click on the `Clear Cookie` button to delete the cookie from the browser
- Open the browser's developer tools and go to the `Application` tab to see the cookies




## server
This is the server for the cookie project. It is built with Express and Node.js. It sets the cookie in the client's browser and also gets the cookie from the client's browser.

## client
This is the client for the cookie project. It is built with React. It sets the cookie in the client's browser and also gets the cookie from the client's browser.

